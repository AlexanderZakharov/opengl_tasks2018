#pragma once

#include <array>
#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"

struct Point{
    int x;
    int y;
};

class TerrainApplication : public Application {
public:
    explicit TerrainApplication(const std::vector<std::vector<glm::vec3>> &location);

    void makeScene() override;

    void draw() override;


private:
    static const std::array<Point, 6> SHIFTS_IN_MATRIX;

    MeshPtr buildMesh();


    std::vector<std::vector<glm::vec3>> calculateNormalsMatrix() const;

    std::vector<std::vector<glm::vec3>> _location;
    std::vector<std::vector<glm::vec3>> _normals;
    MeshPtr _mesh;
    ShaderProgramPtr _shader;


    glm::vec3 quadNormal(const std::vector<Point> &indeces, int triangle_type) const;
};
