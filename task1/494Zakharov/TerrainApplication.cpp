#include "TerrainApplication.hpp"
#include <algorithm>

const std::array<Point, 6> TerrainApplication::SHIFTS_IN_MATRIX = {{{0, 0}, {1, 0}, {0, 1},
                                                                           {0, 1}, {1, 0}, {1, 1}}};

void TerrainApplication::makeScene() {
    Application::makeScene();
    _mesh = buildMesh();
    _mesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
    _shader = std::make_shared<ShaderProgram>("494ZakharovData/shader.vert",
                                              "494ZakharovData/shader.frag");

}

void TerrainApplication::draw() {
    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glPolygonOffset(1.0f, 1.0f);
    _shader->use();
    _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
    _shader->setMat4Uniform("modelMatrix", _mesh->modelMatrix());
    _shader->setIntUniform("lineFlag", 0);
    _mesh->draw();
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    _shader->setIntUniform("lineFlag", 1);
    _mesh->draw();
}

TerrainApplication::TerrainApplication(const std::vector<std::vector<glm::vec3>> &location)
        : _location(location) {
    _cameraMover = std::make_shared<FreeCameraMover>();
    _normals = calculateNormalsMatrix();
}

MeshPtr TerrainApplication::buildMesh() {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    for (int i = 0; i < _location.size() - 1; ++i) {
        for (int j = 0; j < _location[i].size() - 1; ++j) {
            for (auto shift: SHIFTS_IN_MATRIX) {
                int dx = shift.x;
                int dy = shift.y;
                vertices.push_back(_location[i + dx][j + dy]);
                normals.push_back(_normals[i + dx][j + dy]);
            }
        }
    }
    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);

    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

std::vector<std::vector<glm::vec3>> TerrainApplication::calculateNormalsMatrix() const {
    std::vector<std::vector<glm::vec3>> result;
    for (int i = 0; i < _location.size(); ++i) {
        std::vector<glm::vec3> current_line;
        for (int j = 0; j < _location[i].size(); ++j) {
            glm::vec3 vector(0, 0, 0);
            float number_of_neighbours = 0;
            bool first_in_line = (i == 0);
            bool first_in_vertical = (j == 0);
            bool last_in_line = (i + 1 == _location.size());
            bool last_in_vertical = (j + 1 == _location[i].size());
            if (!first_in_line && !first_in_vertical) {
                number_of_neighbours += 2;
                std::vector<Point> current_points = {{i - 1, j - 1},
                                                     {i - 1, j},
                                                     {i,     j},
                                                     {i,     j - 1}};
                vector += quadNormal(current_points, 2);

            }
            if (!first_in_line && !last_in_vertical) {
                number_of_neighbours += 2;
                std::vector<Point> current_points = {{i - 1, j},
                                                     {i - 1, j + 1},
                                                     {i,     j + 1},
                                                     {i,     j}};
                vector += quadNormal(current_points, 1);
            }
            if (!last_in_line && !last_in_vertical) {
                number_of_neighbours += 2;
                std::vector<Point> current_points = {{i,     j},
                                                     {i,     j + 1},
                                                     {i + 1, j + 1},
                                                     {i + 1, j}};
                vector += quadNormal(current_points, 2);

            }
            if (!last_in_line && !first_in_vertical) {
                number_of_neighbours += 2;
                std::vector<Point> current_points = {{i,     j - 1},
                                                     {i,     j},
                                                     {i + 1, j},
                                                     {i + 1, j - 1}};
                vector += quadNormal(current_points, 0);
            }
            vector /= number_of_neighbours;
            current_line.emplace_back(vector);
        }
        result.emplace_back(current_line);
    }
    return result;
}

glm::vec3
TerrainApplication::quadNormal(const std::vector<Point> &indeces, int triangle_type) const {
    glm::vec3 normal;
    if (triangle_type == 0 || triangle_type == 2) {
        normal += glm::normalize(glm::cross(
                _location[indeces[0].x][indeces[0].y] - _location[indeces[1].x][indeces[1].y],
                _location[indeces[2].x][indeces[2].y] - _location[indeces[1].x][indeces[1].y]));
    }
    if (triangle_type == 1 || triangle_type == 2) {
        normal += glm::normalize(glm::cross(
                _location[indeces[3].x][indeces[3].y] - _location[indeces[0].x][indeces[0].y],
                _location[indeces[2].x][indeces[2].y] - _location[indeces[0].x][indeces[0].y]));
    }
    return normal;
}
