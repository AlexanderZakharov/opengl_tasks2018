#include <cmath>
#include <functional>
#include <random>

#include <glm/glm.hpp>

#include "PerlinNoiseGenerator.hpp"


std::vector<std::vector<glm::vec3>>
PerlinNoiseGenerator::generateLocation(float size, int number_of_points) {
    std::vector<std::vector<glm::vec3>> result;
    float step = 2.0f / number_of_points;
    for (int i = 0; i <= number_of_points; ++i) {
        std::vector<glm::vec3> current_line;
        float x = -1.0f + step * i;
        for (int j = 0; j <= number_of_points; ++j) {
            float y = -1.0f + step * j;
            current_line.emplace_back(x * size, y * size, calculatePoint(x, y));
        }
        result.emplace_back(current_line);
    }
    float max = -std::numeric_limits<float>::max();
    float min = std::numeric_limits<float>::max();
    for (auto &line : result) {
        for (auto &vector : line) {
            {
                max = std::max(max, vector[2]);
                min = std::min(min, vector[2]);
            }
        }
    }
    for (auto &line : result) {
        for (auto &vector : line) {
            vector[2] = (vector[2] - min) / (max - min);
        }
    }
    return result;
}

float PerlinNoiseGenerator::calculatePoint(float x, float y, int octaves, float persistence) {
    float amplitude = 1;
    float max = 0;
    float result = 0;

    while (octaves-- > 0) {
        max += amplitude;
        result += calculateNoise(x, y) * amplitude;
        amplitude *= persistence;
        x *= 2;
        y *= 2;
    }

    return result / max;
}

float PerlinNoiseGenerator::calculateNoise(float x, float y) {
    auto left = static_cast<int>(floor(x));
    auto top = static_cast<int>(floor(y));
    float pointInQuadX = x - left;
    float pointInQuadY = y - top;

    glm::vec2 topLeftGradient = getPseudoRandomGradientVector(left, top);
    glm::vec2 topRightGradient = getPseudoRandomGradientVector(left + 1, top);
    glm::vec2 bottomLeftGradient = getPseudoRandomGradientVector(left, top + 1);
    glm::vec2 bottomRightGradient = getPseudoRandomGradientVector(left + 1, top + 1);

    glm::vec2 distanceToTopLeft(pointInQuadX, pointInQuadY);
    glm::vec2 distanceToTopRight(pointInQuadX - 1, pointInQuadY);
    glm::vec2 distanceToBottomLeft(pointInQuadX, pointInQuadY - 1);
    glm::vec2 distanceToBottomRight(pointInQuadX - 1, pointInQuadY - 1);

    float tx1 = glm::dot(distanceToTopLeft, topLeftGradient);
    float tx2 = glm::dot(distanceToTopRight, topRightGradient);
    float bx1 = glm::dot(distanceToBottomLeft, bottomLeftGradient);
    float bx2 = glm::dot(distanceToBottomRight, bottomRightGradient);

    pointInQuadX = qunticCurve(pointInQuadX);
    pointInQuadY = qunticCurve(pointInQuadY);

    float tx = lerp(tx1, tx2, pointInQuadX);
    float bx = lerp(bx1, bx2, pointInQuadX);
    float tb = lerp(tx, bx, pointInQuadY);

    return tb;
}

PerlinNoiseGenerator::PerlinNoiseGenerator() {
    std::random_device rnd_device;
    std::mt19937 engine(rnd_device());
    std::uniform_int_distribution<int> dist(0, 4);
    auto generator = std::bind(dist, engine);
    _permutation_table.resize(1024);
    std::generate(_permutation_table.begin(), _permutation_table.end(), generator);
}

glm::vec2 PerlinNoiseGenerator::getPseudoRandomGradientVector(int x, int y) {
    auto v = static_cast<int>(((x * 1836311903) ^ (y * 2971215073) + 4807526976) & 1023);

    v = _permutation_table[v] & 3;
    switch (v) {
        case 0:
            return {1, 0};
        case 1:
            return {-1, 0};
        case 2:
            return {0, 1};
        default:
            return {0, -1};
    }
}

float PerlinNoiseGenerator::qunticCurve(float t) {
    return t * t * t * (t * (t * 6 - 15) + 10);
}

float PerlinNoiseGenerator::lerp(float a, float b, float t) {
    return a + (b - a) * t;
}