#pragma once

#include <vector>

#include <glm/vec3.hpp>

// Реализация основана на https://habrahabr.ru/post/265775/
class PerlinNoiseGenerator {
public:
    PerlinNoiseGenerator();

    std::vector<std::vector<glm::vec3>> generateLocation(float size, int number_of_points);

private:

    float calculatePoint(float x, float y, int octaves = 8, float persistence = 0.5f);

    float calculateNoise(float x, float y);

    float qunticCurve(float t);

    float lerp(float a, float b, float t);

    glm::vec2 getPseudoRandomGradientVector(int x, int y);

    std::vector<int> _permutation_table;


};