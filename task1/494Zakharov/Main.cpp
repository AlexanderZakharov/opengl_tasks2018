#include <iostream>

#include "TerrainApplication.hpp"
#include "PerlinNoiseGenerator.hpp"


int main(int argc, char* argv[]){
    if (argc == 1) {
        std::cout << "Application started with default params" << std::endl;
        PerlinNoiseGenerator generator;
        auto location = generator.generateLocation(5, 100);
        TerrainApplication terrainApplication(location);
        terrainApplication.start();
    } else {
        std::cout << "Command line arguments are currently not supported" << std::endl;
        return 1;
    }
}